package kz.aitu.team;

import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BOT extends TelegramLongPollingBot {

    public static Map<Long, User> users = new HashMap<Long, User>();


    //status
    //1 - registraciya
    //2 - in game
    //3 - endgame
    public int game_status = 1;


    public void onUpdateReceived(Update update) {
        if(update.hasMessage()) {
            Long chatID = update.getMessage().getChatId();
            String message = update.getMessage().getText();

            if(!users.containsKey(chatID)) {
                send(chatID, "Приветствую новый игрок!");

                User user = new User();
                user.chatID = chatID;
                user.status = 1;
                users.put(user.chatID, user);
            }

            if(users.get(chatID).name == null) {
                send(chatID, "Введите ваше имя. [name YOUR_NAME]");
            } else if(users.get(chatID).secret == null) {
                send(chatID, "Введите кодовое слово. [code YOUR_CODE]");
            }

            User user = users.get(chatID);
            if(message.startsWith("name")) {
                String s = message.substring(5);
                if(message.length() > 5) {
                    user.name = s;
                    users.put(chatID, user);
                }
            } else if(message.startsWith("message")) {
                String s = message.substring(8);
                if (users.get(chatID).name != null) {
                    broadcastMessage(users.get(chatID).name, s);
                }
            } else if(message.startsWith("code")) {
                String s = message.substring(5);
                if(s.length() >= 3) {
                    user.secret = s;
                    users.put(chatID, user);
                }
            } else if(message.startsWith("startgame")) {
                startGame();

                for (User u : users.values()) {
                    send(u.chatID, users.get(u.prayID).name);
                }
            }else if(message.startsWith("endgame")) {
                startGame();
//asdasdasdasd
                for (User u : users.values()) {
                    send(u.chatID, users.get(u.prayID).name);
                }
            }
        }
    }

    private void startGame() {

        Long prayID = null;
        User firstUser = null;
        for (User user: users.values()) {
            if(prayID == null) {
                prayID = user.chatID;
                firstUser = user;
            } else {
                user.prayID = prayID;
                prayID = user.chatID;
            }

        }
        firstUser.prayID = prayID;
    }



    public void send(Long chatID, String text) {
        SendMessage s = new SendMessage();
        s.setText(text);
        s.setChatId(chatID);
        try {
            sendMessage(s);
        } catch (TelegramApiException e){
            //e.printStackTrace();
        }

    }

    public void broadcastMessage(String sender, String text) {
        for (User user: users.values()) {
            send(user.chatID, sender + ": " + text);
        }
    }

    public String getBotUsername() {
        return "team";
    }

    public String getBotToken() {
        return "925616493:AAEquK6hMR5qKm0I8PpxvJmMGHFup6R3lb8";
    }
}
