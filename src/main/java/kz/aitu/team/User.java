package kz.aitu.team;

public class User {

    Long chatID;
    String name = null;
    Integer status = 1;

    String secret;
    Long prayID;

    @Override
    public String toString() {
        return "User{" +
                "chatID=" + chatID +
                ", name='" + name + '\'' +
                ", status=" + status +
                ", secret='" + secret + '\'' +
                ", prayID=" + prayID +
                '}';
    }
}
